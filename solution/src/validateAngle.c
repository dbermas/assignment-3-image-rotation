#define OK 1
#define ERROR 0
#define ONE_ROTATION_IN_DEGREE 90
#define FULL_ROTATION_IN_DEGREE 360


int validateValue(int angle){
    return (((angle + FULL_ROTATION_IN_DEGREE) % ONE_ROTATION_IN_DEGREE == 0 && angle >= -270 && angle <= 270)  ? OK : ERROR);
}

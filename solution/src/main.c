#include "../include/bmpIO.h"
#include "../include/deserializer.h"
#include "../include/imageFunctions.h"
#include "../include/serializer.h"
#include "../include/validateAngle.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stderr, "Syntax: bmpreader <bmp file> <destination file> <angle>\n");
        return 0;
    }
    int angle = atoi(argv[3]);
    if (!validateValue(angle)) {
        fprintf(stderr, "Error: permitted values: 0, 90, 180, 270, -90, -180, 270");
    }

    char *fileName = argv[1];
    struct image openImage;
    enum read_status openBMPStatus = openBMP(fileName);
    FILE *in = 0;
    if (openBMPStatus == READ_OK) {
        in = fopen(fileName, "rb");
    } else {
        fclose(in);
        exit(1);
    }

    enum read_status openImageStatus = fromBMP(in, &openImage);
    if (openImageStatus != READ_OK) {
        free(openImage.data);
        exit(1);
    }
    fclose(in);

    openImage = rotate(openImage, angle);

    char *outputFileName = argv[2];
    FILE *out = 0;
    enum write_status writeBMPStatus = writeBMP(outputFileName);

    if (writeBMPStatus == WRITE_OK) {
        out = fopen(outputFileName, "wb");
    } else {
        fclose(out);
        exit(1);
    }
    enum write_status writeImageStatus = toBMP(out, &openImage);
    if (writeImageStatus != WRITE_OK) {
        free(openImage.data);
        fprintf(stderr, "Error: can not write image");
        exit(1);

    }

    fclose(out);
    free(openImage.data);
    return 0;
}

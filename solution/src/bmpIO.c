#include "../include/deserializer.h"
#include "../include/serializer.h"
#include <stdio.h>






enum read_status openBMP(char const *fileName) {
    FILE *fp = fopen(fileName, "rb");
    if (!fp) {
        fprintf(stderr,"Can't load file \'%s'\n", fileName);
        return READ_ERROR;
    }
    fclose(fp);
    return READ_OK;
}
enum write_status writeBMP(char const *fileName) {
    FILE *fp = fopen(fileName, "wb");
    if (!fp) {
        fprintf(stderr,"Can't write file \'%s'\n", fileName);
        return WRITE_ERROR;
    }
    fclose(fp);

    return WRITE_OK;
}



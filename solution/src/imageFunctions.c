#include "../include/image.h"
#include <stdlib.h>


#define FULL_ROTATION 4
#define ONE_ROTATION_IN_DEGREE 90
#define FULL_ROTATION_IN_DEGREE 360

struct image transposition(struct image imageToRotateOnce) {
    struct image tempImage;
    const size_t WIDTH = imageToRotateOnce.width;
    const size_t HEIGHT = imageToRotateOnce.height;
    tempImage = createImage(HEIGHT, WIDTH);

    for (size_t i = 0; i < HEIGHT; i++) {
        for (size_t j = 0; j < WIDTH; j++) {
            tempImage.data[(WIDTH-j-1)*HEIGHT+i]=imageToRotateOnce.data[i*WIDTH+j];
        }
    }
    free(imageToRotateOnce.data);
    return tempImage;
}

struct image reTransposition(struct image imageToRotateOnce) {
    struct image tempImage;
    const size_t WIDTH = imageToRotateOnce.width;
    const size_t HEIGHT = imageToRotateOnce.height;
    tempImage = createImage(HEIGHT, WIDTH);

    for (size_t i = 0; i < HEIGHT; i++) {
        for (size_t j = 0; j < WIDTH; j++) {
            tempImage.data[HEIGHT-1-i+j*HEIGHT]=imageToRotateOnce.data[j+i*WIDTH];
        }
    }
    free(imageToRotateOnce.data);
    return tempImage;
}
struct image mirror(struct image imageToRotateOnce) {
    struct image tempImage;
    const size_t WIDTH = imageToRotateOnce.width;
    const size_t HEIGHT = imageToRotateOnce.height;
    tempImage = createImage(WIDTH,  HEIGHT);
    for (size_t i = 0; i < HEIGHT; i++) {
        for (size_t j = 0; j < WIDTH; j++) {
            tempImage.data[(WIDTH-j-1)+(HEIGHT-i-1)*WIDTH]=imageToRotateOnce.data[j+i*WIDTH];
        }
    }
    free(imageToRotateOnce.data);
    return tempImage;
}
struct image rotate(struct image imageToRotate, int const angle) {
    int rotationCount = ((angle + FULL_ROTATION_IN_DEGREE) / ONE_ROTATION_IN_DEGREE) % FULL_ROTATION;

    switch (rotationCount) {
        case 1:
            imageToRotate = transposition(imageToRotate);
            break;
        case 2:
            imageToRotate = mirror(imageToRotate);
            break;
        case 3:
            imageToRotate = reTransposition(imageToRotate);
            break;

    }
    return imageToRotate;
}

#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>



struct image createImage(uint64_t const width, uint64_t const height) {
    struct image newImage;

    newImage.width = width;
    newImage.height = height;
    newImage.data = (struct pixel*)malloc(sizeof(struct pixel) * width * height);
    if (newImage.data == NULL){
        fprintf(stderr,"Error: memory allocation error");
    }

    return newImage;
}

#include <stdint.h>
#define PADDING 4

uint8_t countPadding(uint64_t const width){
    return  (PADDING - width%PADDING)%PADDING;
}

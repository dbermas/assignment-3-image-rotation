#include "../include/image.h"
#include <stdio.h>


#ifndef IMAGE_ROTATION_SERIALIZER_H
#define IMAGE_ROTATION_SERIALIZER_H


enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,

};

enum write_status toBMP( FILE *out, struct image const*  img );
#endif //IMAGE_ROTATION_SERIALIZER_H

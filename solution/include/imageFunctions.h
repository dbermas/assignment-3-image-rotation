#include "../include/image.h"

#ifndef IMAGE_ROTATION_IMAGEFUNCTIONS_H
#define IMAGE_ROTATION_IMAGEFUNCTIONS_H

struct image rotate(struct image,int const angle);


#endif //IMAGE_ROTATION_IMAGEFUNCTIONS_H
